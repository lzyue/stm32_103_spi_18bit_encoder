#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "usart.h"	 	 
#include "spi.h"
#define	Encoder_18 		PBout(12)  		//Encoder_18片选信号

	void Encoder_18_init()
	{
		GPIO_InitTypeDef GPIO_InitStructure;
		RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );//PORTB时钟使能 

		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;  // PB12 推挽 
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;  //推挽输出
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		GPIO_SetBits(GPIOB,GPIO_Pin_12);
	}
 int main(void)
 {	 
	u8 t;
	u8 len;	
	u16 times=0;  
	int H03=0;
	int H04=0;
	int H05=0;
	int angle=0;
	 

	 
	delay_init();	    	 //延时函数初始化	  
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
	uart_init(115200);	 	//串口初始化为115200
	LED_Init();		  		//初始化与LED连接的硬件接口	
	KEY_Init();				//按键初始化		 
  Encoder_18_init();	 
  Encoder_18=1;				//SPI FLASH不选中
	SPI2_Init();		   	//初始化SPI
	SPI2_SetSpeed(SPI_BaudRatePrescaler_128);//设置为18M时钟,高速模式

	while(1)
	{
		if(USART_RX_STA&0x8000)
		{					   
			len=USART_RX_STA&0x3fff;//得到此次接收到的数据长度
			printf("\r\n发送数据:\r\n");
			for(t=0;t<len;t++)
			{
				USART_SendData(USART1, USART_RX_BUF[t]);         //向串口1发送数据
				while(USART_GetFlagStatus(USART1,USART_FLAG_TC)!=SET);//等待发送结束
			}
			printf("\r\n\r\n");//插入换行
			USART_RX_STA=0;
		}else
		{
			times++;
			if(times%5000==0)
			{
				printf("\r\n 18线单圈编码器测试代码\r\n");
			}
			  Encoder_18=0;			//SPI
	      SPI2_ReadWriteByte(0x83);	//6825 Angle  command 
	      H03=SPI2_ReadWriteByte(0x00);	//6825???  H  
	      H04=SPI2_ReadWriteByte(0x00); //M
	      H05=SPI2_ReadWriteByte(0x00);	//L
        Encoder_18=1;	
			   
			  angle=(((H03&0x00ff)<<10)|((H04&0x00fc)<<2)|((H05&0x00f0)>>4))&0x3ffff;
         			
			//angle = SPI_RW();  //5040读角度
			if(times%200==0)printf("角度值H:%x\r\n",H03); //输出角度 
			if(times%200==0)printf("角度值M:%x\r\n",H04); //输出角度 
			if(times%200==0)printf("角度值L:%x\r\n",H05); //输出角度 
			if(times%200==0)printf("RAW:%d  Angle_Rad:%f Angle_Dec: %f \r\n",angle,(angle/262144.0)*360.0*3.141592653/180.0,(angle/262144.0)*360.0); //输出角度 
			if(times%30==0)LED0=!LED0;//闪烁LED,提示系统正在运行.
			delay_ms(10);   
		}
	}
}


